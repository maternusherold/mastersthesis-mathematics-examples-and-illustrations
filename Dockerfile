FROM python:3.9.10-slim

#install poetry for dependency management
RUN pip install poetry

WORKDIR /home/surrogate-modelling-in-high-dim-examples

#copy file specifying the dependencies as well as desired versions
COPY pyproject.toml .
COPY poetry.lock .

#copy application code
COPY src ./src

#update image and install python dependencies
RUN apt update && \
    python -m pip install --upgrade pip && \
    pip install poetry && \
    rm -rf /var/lib/apt/lists/* && \
    poetry config virtualenvs.in-project true && \
    poetry install -vv
