# Examples and Illustrations in My Master's Thesis


Within this repository, I publish the code used to generate the illustrations and compute examples, which were used in my master's thesis for my degree in mathematics. 

The required python packages can be installed by running `poetry install`, requiring the installation of the poetry dependency management tool. 

For ease of use, a Docker image will be provided, tacking care of the environment setup.


## Usage

Execute the Docker image using 

```bash
sudo docker run -it -p 8888:8888 registry.gitlab.com/maternusherold/mastersthesis-mathematics-examples-and-illustrations bash
```

Inside the running container, use `poetry run jupyter notebook --allow-root` to start the Jupyter server. The port mapping in the above command allows to access the GUI using the `localhost:8888` address.


## todos...

 - [x] Dockerfile
 - [x] Provided Docker image via repo's registry

